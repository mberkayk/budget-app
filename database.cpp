#include "database.h"
#include <QStandardPaths>
#include <QDir>
#include <QDebug>

Database::Database(): database() {
    QString dirStr = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(dirStr);
    if(!dir.exists()){
        qDebug() << "data directory does not exist";
        if(dir.mkpath(dirStr)) qDebug() << "data directory was created";
        else qDebug() << "data directory couldn't be created";
    }else{
        qDebug() << "data directory alredy exists" << dirStr;
    }

    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(dirStr+"/Budget-App-Database.db");
    if(!database.open()) qDebug() << database.lastError();

    QSqlQuery q(database);
    if(!database.tables().contains("daily_spendings")){
        q.exec("CREATE TABLE daily_spendings(date INTEGER, desc TEXT, amt INTEGER)");
    }
    if(!database.tables().contains("weekly_spendings")){
        q.exec("CREATE TABLE weekly_spendings(date INTEGER, desc TEXT, amt INTEGER)");
    }
    if(!database.tables().contains("monthly_spendings")){
        q.exec("CREATE TABLE monthly_spendings(id INTEGER PRIMARY KEY, desc TEXT, amt INTEGER)");
    }
    if(!database.tables().contains("month")){
        q.exec("CREATE TABLE month(date INTEGER PRIMARY KEY, budget INTEGER)");
    }
    if(!database.tables().contains("month_and_monthly_spendings")){
        q.exec("CREATE TABLE month_and_monthly_spendings(month INTEGER, id INTEGER)");
    }
}

Database::~Database(){
    database.close();
}

QVector<Entry*> Database::getDayEntries(QDate &date){
    QSqlQuery q(database);
    QString day = QString::number(date.toJulianDay());
    q.exec("SELECT desc, amt FROM daily_spendings WHERE date="+day);


    QVector<Entry*> result;
    QSqlRecord rec;
    while(q.next()){
        rec = q.record();
        result.append(new Entry(rec.value("amt").toInt(), rec.value("desc").toString()));
    }
    return result;
}

QVector<Entry*> Database::getWeekEntries(QDate &date){
    if(date.dayOfWeek() != 1){
        qDebug() << date.dayOfWeek();
        qDebug() << "day indicating week must be a monday";
        return QVector<Entry*>();
    }

    QSqlQuery q(database);
    QString day = QString::number(date.toJulianDay());
    q.exec("SELECT desc, amt FROM weekly_spendings WHERE date="+day);


    QVector<Entry*> result;
    QSqlRecord rec;
    while(q.next()){
        rec = q.record();
        result.append(new Entry(rec.value("amt").toInt(), rec.value("desc").toString()));
    }
    return result;
}

int Database::getMonthlyBudget(QDate &date){
    if(date.day() != 1){
        qDebug() << date.day();
        qDebug() << "day indicating month must be the first day of the month";
        return -1;
    }

    QSqlQuery q(database);
    QString day = QString::number(date.toJulianDay());
    q.exec("SELECT budget FROM month WHERE date="+day);

    q.next();
    return q.record().value("budget").toInt();
}

QVector<Entry*> Database::getMonthEntries(QDate &date){
    if(date.day() != 1){
        qDebug() << date.day();
        qDebug() << "day indicating month must be the first day of the month";
        return QVector<Entry*>();
    }

    QSqlQuery q(database);
    QString day = QString::number(date.toJulianDay());
    q.exec("SELECT id FROM month_and_monthly_spendings WHERE month="+day);

    QVector<int> ids;
    while(q.next()){
        ids.append(q.record().value("id").toInt());
    }

    QVector<Entry*> result;
    for(int i = 0; i < ids.length(); i++){
        q.exec("SELECT desc,amt FROM monthly_spendings WHERE id="+QString::number(ids[i]));
        q.next();
        QSqlRecord rec = q.record();
        result.append(new Entry(rec.value("amt").toInt(), rec.value("desc").toString()));
    }
    return result;
}

void Database::appendDayEntries(QDate &date, QVector<Entry *> v) {

    QString day = QString::number(date.toJulianDay());
    QSqlQuery q(database);
    for(int i = 0; i < v.size(); i++){
        QString desc = "'" + v[i]->getDesc() + "'";
        QString amt = QString::number(v[i]->getAmount());
        q.exec("INSERT INTO daily_spendings VALUES(" + day + ","
            + desc + "," + amt + ")");
    }

}

void Database::appendWeekEntries(QDate &date, QVector<Entry *> v) {
    if(date.dayOfWeek() != 1){
        qDebug() << date.dayOfWeek();
        qDebug() << "day indicating week must be the first day of the week";
        return;
    }

    QString day = QString::number(date.toJulianDay());
    QSqlQuery q(database);
    for(int i = 0; i < v.size(); i++){
        QString desc = "'" + v[i]->getDesc() + "'";
        QString amt = QString::number(v[i]->getAmount());
        q.exec("INSERT INTO weekly_spendings VALUES(" + day + ","
            + desc + "," + amt + ")");
    }

}

void Database::setMonthlyBudget(QDate &date, int budget){
    if(date.day() != 1){
        qDebug() << date.day();
        qDebug() << "day indicating month must be the first day of the month";
        return;
    }

    QString day = QString::number(date.toJulianDay());
    QSqlQuery q(database);

    q.exec("SELECT * FROM month WHERE date=" + day);
    if(q.next()){
        q.exec("UPDATE month SET budget=" + QString::number(budget));
    }else{ // month doesn't exist in the database
        q.exec("INSERT INTO month VALUES(" + day + "," + QString::number(budget) + ")");
    }
}

void Database::appendMonthEntries(QDate &date, QVector<Entry *> v){
    if(date.day() != 1){
        qDebug() << date.day();
        qDebug() << "day indicating month must be the first day of the month";
        return;
    }

    QString day = QString::number(date.toJulianDay());

    QSqlQuery q(database);
    for(int i = 0; i < v.size(); i++){
        QString desc = "'" + v[i]->getDesc() + "'";
        QString amt = QString::number(v[i]->getAmount());

        q.exec("SELECT id FROM monthly_spendings WHERE desc=" + desc + " AND amt="+amt);
        if(q.next() == false){ //if record doesn't already exist
            QString cmd ="INSERT INTO monthly_spendings(desc,amt) VALUES(" + desc + "," + amt + ")";
            q.exec(cmd);
        }
        q.exec("SELECT id FROM monthly_spendings WHERE desc=" + desc + " AND amt="+amt);
        q.next();
        int id = q.record().value("id").toInt();

        q.exec("INSERT INTO month_and_monthly_spendings VALUES("+day+","+QString::number(id) +")");
    }

}


void Database::removeDailyEntry(QDate &date, Entry* entry){
    QSqlQuery q(database);

    QString day = QString::number(date.toJulianDay());
    QString desc = entry->getDesc();
    QString amt = QString::number(entry->getAmount());

    q.exec("DELETE FROM daily_spendings WHERE date="+day
           +" AND desc="+desc+" AND amt="+amt+" LIMIT 1");
}

void Database::removeWeeklyEntry(QDate &date, Entry *entry){
    if(date.dayOfWeek() != 1){
        qDebug() << date.dayOfWeek();
        qDebug() << "day indicating week must be the first day of the week";
        return;
    }
    QSqlQuery q(database);

    QString day = QString::number(date.toJulianDay());
    QString desc = entry->getDesc();
    QString amt = QString::number(entry->getAmount());

    q.exec("DELETE FROM weekly_spendings WHERE date="+day
           +" AND desc="+desc+" AND amt="+amt+" LIMIT 1");
}

void Database::removeMonthlyEntry(QDate &date, Entry* entry){
    if(date.day() != 1){
        qDebug() << date.day();
        qDebug() << "day indicating month must be the first day of the month";
        return;
    }

    QSqlQuery q(database);

    QString day = QString::number(date.toJulianDay());
    QString desc = entry->getDesc();
    QString amt = QString::number(entry->getAmount());

    QString id;
    q.exec("SELECT id FROM monthly_spendings WHERE date="+day+" AND desc="+desc+" AND amt="+amt);
    QSqlRecord rec;
    if(q.next()) {
        rec = q.record();
        id = rec.value("id").toString();
    }else {
        qDebug() << "monthly spending doesn't exist in the database!";
        return;
    }


    q.exec("DELETE FROM month_and_monthly_spendings WHERE date="+day
           +"AND id="+ id +" LIMIT 1");
}
