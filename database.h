#ifndef DATABASE_H
#define DATABASE_H

#include <QDate>
#include <QFile>
#include <QtSql>

#include "entrygroup.h"

class Database {

public:
    Database();
    ~Database();

    QVector<Entry*> getDayEntries(QDate&);
    double getWeeklyBudget(QDate&);
    QVector<Entry*> getWeekEntries(QDate&);
    int getMonthlyBudget(QDate&);
    QVector<Entry*> getMonthEntries(QDate&);

    void appendDayEntries(QDate&, QVector<Entry*>);
    void appendWeekEntries(QDate&, QVector<Entry*>);
    void setMonthlyBudget(QDate&, int);
    void appendMonthEntries(QDate&, QVector<Entry*>);

    void removeDailyEntry(QDate&, Entry*);
    void removeWeeklyEntry(QDate&, Entry*);
    void removeMonthlyEntry(QDate&, Entry*);


private:

    QSqlDatabase database;

};

#endif // DATABASE_H
